export function getRandomFromTo(min, max) {
  return Math.random() * (max - min) + min;
}