import { fight } from '../components/fight';
import { createElement } from '../helpers/domHelper';
import { createFighterImage } from './fighterPreview';
import { showWinnerModal } from './modal/winner';

const asyncSetTimeout = (callback, delay) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      callback();
      resolve();
    }, delay);
  });
};

export async function showTimer() {
  let number = 3;
  let dots = 0;

  while (number > 0) {
    const id = setInterval(() => {
      showTimer.element.innerText = `${number}${'.'.repeat(++dots)}`;
      dots = dots >= 3 ? 0 : dots;
    }, 300);

    await asyncSetTimeout(() => {
      clearInterval(id);
      number--;
      showTimer.element.innerText = `${number}${'.'.repeat(++dots)}`;
    }, 1000);
  }

  showTimer.element.innerText = 'START!!!';
  showTimer.element.classList.add('move-up-animation');
  setTimeout(() => {
    showTimer.element.remove();
  }, 1000);
}

export async function renderArena(selectedFighters) {
  const root = document.getElementById('root');
  const arena = createArena(selectedFighters);

  root.innerHTML = '';
  root.append(arena);

  await showTimer();
  const winner = await fight(...selectedFighters);

  showWinnerModal(winner);
  // todo:
  // - start the fight
  // - when fight is finished show winner
}

function createArena(selectedFighters) {
  const arena = createElement({ tagName: 'div', className: 'arena___root' });
  const healthIndicators = createHealthIndicators(...selectedFighters);
  const fighters = createFighters(...selectedFighters);
  const timerBeforeStartFight = createTimerBeforeStartFight();

  showTimer.element = timerBeforeStartFight;

  arena.append(healthIndicators, fighters, timerBeforeStartFight);
  return arena;
}

function createHealthIndicators(leftFighter, rightFighter) {
  const healthIndicators = createElement({ tagName: 'div', className: 'arena___fight-status' });
  const versusSign = createElement({ tagName: 'div', className: 'arena___versus-sign' });
  const leftFighterIndicator = createHealthIndicator(leftFighter, 'left');
  const rightFighterIndicator = createHealthIndicator(rightFighter, 'right');

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter, position) {
  const { name } = fighter;
  const container = createElement({ tagName: 'div', className: 'arena___fighter-indicator' });
  const fighterName = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  const indicator = createElement({ tagName: 'div', className: 'arena___health-indicator' });
  const bar = createElement({
    tagName: 'div',
    className: 'arena___health-bar',
    attributes: { id: `${position}-fighter-indicator` },
  });
  const powerIndicator = createElement({ tagName: 'div', className: 'arena___power-indicator' });
  const powerBar = createElement({
    tagName: 'div',
    className: 'arena___power-bar',
    attributes: { id: `${position}-fighter-power-indicator` },
  });

  fighterName.innerText = name;
  indicator.append(bar);
  powerIndicator.append(powerBar);
  container.append(fighterName, indicator, powerIndicator);

  return container;
}

function createFighters(firstFighter, secondFighter) {
  const battleField = createElement({ tagName: 'div', className: `arena___battlefield` });
  const firstFighterElement = createFighter(firstFighter, 'left');
  const secondFighterElement = createFighter(secondFighter, 'right');

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter, position) {
  const imgElement = createFighterImage(fighter);
  const positionClassName = position === 'right' ? 'arena___right-fighter' : 'arena___left-fighter';
  const fighterElement = createElement({
    tagName: 'div',
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}

function createTimerBeforeStartFight() {
  const timerElement = createElement({
    tagName: 'div',
    className: `arena___timer`,
  });

  return timerElement;
}
