import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import App from '../../app';

export function showWinnerModal(fighter) {
  const bodyElement = createElement({
    tagName: 'div',
    className: 'modal__winner-body',
  });

  bodyElement.innerHTML = `
    <img src="${fighter.source}" alt="${fighter.name}" class="fighter-img">
    <img src="./resources/winner-small.png" alt="winner">
  `;

  const onClose = () => {
    location.reload();
  };

  const title = `${fighter.name} (left ${Math.floor(fighter.health)}HP from ${fighter.fullHealth}HP)`;

  showModal({ title, bodyElement, onClose });
  // call showModal function
}
