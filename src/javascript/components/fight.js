import { controls, playerOneAllowedKeys, playerTwoAllowedKeys } from '../../constants/controls';
import { getRandomFromTo } from '../helpers/random';

const RECHARGING_CRITICAL_TIME_MS = 10000;

const {
  PlayerOneAttack,
  PlayerTwoAttack,
  PlayerOneBlock,
  PlayerTwoBlock,
  PlayerOneCriticalHitCombination,
  PlayerTwoCriticalHitCombination,
} = controls;

function setInitialFightState(firstFighter, secondFighter) {
  firstFighter.fullHealth = firstFighter.health;
  secondFighter.fullHealth = secondFighter.health;

  if (!fight.leftPowerIndicator) {
    fight.leftPowerIndicator = document.getElementById('left-fighter-power-indicator');
  }

  if (!fight.rightPowerIndicator) {
    fight.rightPowerIndicator = document.getElementById('right-fighter-power-indicator');
  }

  fight.leftPowerIndicator.classList.add('power-bar--filling-active');
  fight.rightPowerIndicator.classList.add('power-bar--filling-active');

  setTimeout(() => {
    firstFighter.isCanUseCriticalHit = true;
    secondFighter.isCanUseCriticalHit = true;
  }, RECHARGING_CRITICAL_TIME_MS);
}

export async function fight(firstFighter, secondFighter) {
  let playerOnePressedKeys = [];
  let playerTwoPressedKeys = [];

  setInitialFightState(firstFighter, secondFighter);

  return new Promise((resolve) => {
    function onKeyDown(e) {
      if (playerOnePressedKeys.includes(e.code) || playerTwoPressedKeys.includes(e.code)) {
        return;
      }

      if (playerOneAllowedKeys.includes(e.code)) {
        playerOnePressedKeys.push(e.code);
      }

      if (playerTwoAllowedKeys.includes(e.code)) {
        playerTwoPressedKeys.push(e.code);
      }

      switch (e.code) {
        case PlayerOneAttack:
          {
            if (playerOnePressedKeys.includes(PlayerOneBlock) || playerTwoPressedKeys.includes(PlayerTwoBlock)) {
              break;
            }
            console.log('check');
            const damage = getDamage(firstFighter, secondFighter);
            renderDamageFor(secondFighter, 'right', damage);
          }
          break;
        case PlayerTwoAttack:
          {
            if (playerOnePressedKeys.includes(PlayerOneBlock) || playerTwoPressedKeys.includes(PlayerTwoBlock)) {
              break;
            }

            const damage = getDamage(secondFighter, firstFighter);
            renderDamageFor(firstFighter, 'left', damage);
          }
          break;
        default: {
          const isPlayerOneCriticalHit = PlayerOneCriticalHitCombination.every((key) =>
            playerOnePressedKeys.includes(key)
          );

          if (isPlayerOneCriticalHit && firstFighter.isCanUseCriticalHit) {
            const damage = firstFighter.attack * 2;
            renderDamageFor(secondFighter, 'right', damage);
            rechargeCriticalPowerFor(firstFighter, 'left');
            break;
          }

          const isPlayerTwoCriticalHit = PlayerTwoCriticalHitCombination.every((key) =>
            playerTwoPressedKeys.includes(key)
          );

          if (isPlayerTwoCriticalHit && secondFighter.isCanUseCriticalHit) {
            const damage = secondFighter.attack * 2;
            renderDamageFor(firstFighter, 'left', damage);
            rechargeCriticalPowerFor(secondFighter, 'right');
          }
        }
      }

      if (firstFighter.health <= 0) {
        clearEventListeners();
        resolve(secondFighter);
      }

      if (secondFighter.health <= 0) {
        clearEventListeners();
        resolve(firstFighter);
      }
    }

    function onKeyUp(e) {
      switch (e.code) {
        case PlayerOneBlock:
          {
            firstFighter.isInBlockMode = false;
          }
          break;
        case PlayerTwoBlock:
          {
            secondFighter.isInBlockMode = false;
          }
          break;
      }

      playerOnePressedKeys = playerOnePressedKeys.filter((pressedKey) => pressedKey !== e.code);
      playerTwoPressedKeys = playerTwoPressedKeys.filter((pressedKey) => pressedKey !== e.code);
    }

    function clearEventListeners() {
      window.removeEventListener('keydown', onKeyDown);
      window.removeEventListener('keyup', onKeyUp);
    }
    // resolve the promise with the winner when fight is over
    window.addEventListener('keydown', onKeyDown);
    window.addEventListener('keyup', onKeyUp);
  });
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  const damage = hitPower - blockPower;

  return damage < 0 ? 0 : damage;
}

export function renderDamageFor(fighter, position, damage) {
  fighter.health -= damage;
  const healthPercents = fighter.health > 0 ? `${(fighter.health / fighter.fullHealth) * 100}%` : '0';

  if (!renderDamageFor[position]) {
    renderDamageFor[position] = document.getElementById(`${position}-fighter-indicator`);
  }

  renderDamageFor[position].style.width = healthPercents;
}

export function getHitPower(fighter) {
  const criticalHitChance = getRandomFromTo(1, 2);
  const hitPower = fighter.attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  const dodgeChance = getRandomFromTo(1, 2);
  const blockPower = fighter.defense * dodgeChance;
  return blockPower;
}

function rechargeCriticalPowerFor(fighter, position) {
  const element = fight[`${position}PowerIndicator`];
  fighter.isCanUseCriticalHit = false;

  element.classList.remove('power-bar--filling-active');
  void element.offsetWidth;
  element.classList.add('power-bar--filling-active');

  setTimeout(() => {
    fighter.isCanUseCriticalHit = true;
  }, RECHARGING_CRITICAL_TIME_MS);
}
