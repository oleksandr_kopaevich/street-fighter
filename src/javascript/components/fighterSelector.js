import versusImg from '../../../resources/versus.png';
import { createElement } from '../helpers/domHelper';
import { fightersDetails } from '../helpers/mockData';
import { fighterService } from '../services/fightersService';
import { renderArena } from './arena';
import { createFighterPreview } from './fighterPreview';

export function createFightersSelector() {
  const previewData = {
    selectedFighters: [],
    selectionCursor: null,
  };

  return async (event, fighterId) => {
    const fighter = await getFighterInfo(fighterId);

    if (previewData.selectionCursor === 'left') {
      previewData.selectedFighters[0] = fighter;
    }

    if (previewData.selectionCursor === 'right') {
      previewData.selectedFighters[1] = fighter;
    }

    if (!previewData.selectionCursor) {
      previewData.selectedFighters = [fighter];
      previewData.selectionCursor = 'right';
    }

    renderSelectedFighters(previewData);
  };
}

const fighterDetailsMap = new Map();

export async function getFighterInfo(fighterId) {
  if (fighterDetailsMap.has(fighterId)) {
    return fighterDetailsMap.get(fighterId);
  }

  const fighterDetails = await fighterService.getFighterDetails(fighterId);
  fighterDetailsMap.set(fighterId, fighterDetails);
  return fighterDetails;
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap
}

function renderSelectedFighters(previewData) {
  if (!previewData.rootSelector) {
    previewData.rootSelector = document.querySelector('.preview-container___root');
  }

  const [playerOne, playerTwo] = previewData.selectedFighters;
  const firstPreview = createFighterPreview(playerOne, 'left', previewData);
  const secondPreview = createFighterPreview(playerTwo, 'right', previewData);
  const versusBlock = createVersusBlock(previewData.selectedFighters);

  if (!previewData.selectedFighters[1] && previewData.selectionCursor === 'right') {
    previewData.rootSelector.classList.add(`fighter-preview--active-${previewData.selectionCursor}`);
  }

  previewData.rootSelector.innerHTML = '';
  previewData.rootSelector.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters) {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({ tagName: 'div', className: 'preview-container___versus-block' });
  const image = createElement({
    tagName: 'img',
    className: 'preview-container___versus-img',
    attributes: { src: versusImg },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: 'button',
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters) {
  renderArena(selectedFighters);
}
