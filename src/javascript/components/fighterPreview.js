import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position, previewData) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    fighterElement.innerHTML = `
    <h2 class="fighter-preview___name">${fighter.name}</h2>
    <div class="fighter-preview___image-wrapper">
      <img src="${fighter.source}" alt="${fighter.name}"/>
    </div>
    <div class="fighter-preview___stats-wrap">
      <img src="./resources/attack.svg" alt="attack"/> ${fighter.attack}
      <img src="./resources/defense.svg" alt="defense"/> ${fighter.defense}
      <img src="./resources/health.svg" alt="health"/> ${fighter.health}
    </div>
  `;
  }

  fighterElement.addEventListener('click', () => {
    if (previewData.selectionCursor !== position) {
      const classToRemove = `fighter-preview--active-${previewData.selectionCursor}`;
      previewData.rootSelector.classList.remove(classToRemove);
    }

    previewData.selectionCursor = position;
    previewData.rootSelector.classList.add(`fighter-preview--active-${previewData.selectionCursor}`);
  });

  // todo: show fighter info (image, name, health, etc.)

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
