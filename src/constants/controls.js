export const controls = {
  PlayerOneAttack: 'KeyA',
  PlayerOneBlock: 'KeyD',
  PlayerTwoAttack: 'KeyJ',
  PlayerTwoBlock: 'KeyL',
  PlayerOneCriticalHitCombination: ['KeyQ', 'KeyW', 'KeyE'],
  PlayerTwoCriticalHitCombination: ['KeyU', 'KeyI', 'KeyO'],
};

export const playerOneAllowedKeys = ['KeyA', 'KeyD', 'KeyQ', 'KeyW', 'KeyE'];
export const playerTwoAllowedKeys = ['KeyJ', 'KeyL', 'KeyU', 'KeyI', 'KeyO'];
